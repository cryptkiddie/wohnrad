use <../ressources/modules.scad>;
module bike_rear(insert_wheel=false,insert_mudguard=false,degree=0,framecolor=[0.775,0.299,0.071],main_tubes=true) {
    translate([0,0,30]) union() {
            color(framecolor) {
            //Seat Post
            translate([0,-40,4]) rotate([17,0,0]) resize([6,4,53.3]) cylinder(53.5,3,3);
            if (main_tubes) {
                //Down Tube
                translate([0,-36,1]) rotate([-43,0,0]) resize([6,4,72.5]) cylinder(72.5,3,3);
                //Upper Tube
                translate([0,-55,53.2]) rotate([-81.5,0,0]) resize([6,3,66]) cylinder(72.5,3,3);
                //Steering Tube
                translate([0,15,50]) rotate([20,0,0]) cylinder(17,3,3);
            }
        }
            
            //Seat
            translate([0,-55.5,55]) {
                //Post
                color([0.642,0.595,0.595]) rotate([16,0,0]) cylinder(17,1.5,1.5,false);
                //Saddle
                translate([0,-7,12]) rotate([0,0,180]) color([0.1,0.1,0.1]) saddle();
                }
            
            //back wheel mount
            color(framecolor) {
                //upper
                {
                    //right
                    translate([7,-89.3,7.1]) rotate([-50,180,0]) translate([0,0,0]) rear_tube(25,22);
                    //left
                    translate([-7,-88.2,5.1]) rotate([50,0,0]) rear_tube(25.7,22);
                }
                //lower
                {
                    //right
                    translate([7,-89,7.8]) rotate([7+3,180,0]) translate([0,0,0]) rear_tube(20,20);
                    //left
                    translate([-7,-89.5,5.1]) rotate([-6-3,0,0]) rear_tube(20,20);
                }
                //pedal mount
               difference() {
                   translate ([0,-39.2,-1])rotate ([0,90,0]) cylinder(7,5,5,true);
                   translate ([0,-39.2,-1])rotate ([0,90,0]) cylinder(7.1,2.2,2.2,true);
                   }
            }
            //pedals and mechanics
            {
            //axis
            translate ([0,-88,6.25])rotate ([0,90,0]) cylinder(15,2,2,true);
            //axis pedal

            }
        }
        translate([-5,-88,36.25]) {
             if (insert_wheel) wheel(degree);
            if (insert_mudguard) color([0.18,0.18,0.18]) mudguard(-10,170);
    }
   
}
module groundframes(rs=0,rc=[0,0],slideout=0) {
        roundground(60,rc=rc[0]/*,size=70*/);
        translate([-slideout,60,0]) roundground(55,3,rs=rs);
        //comparment begin
        translate([0,59,0]) rotate([0,90,90])  linear_extrude(1) difference() {
           resize([19,69])circle(d=20);
           polygon([[0,0],[10,0],[10,-35],[-10,-35],[-10,35],[0,35]]);
        }
        //compartment end
        translate([0,115,0]) rotate([0,90,90])  linear_extrude(1) difference() {
           resize([19,69])circle(d=20);
           polygon([[0,0],[10,0],[10,-35],[-10,-35],[-10,35],[0,35]]);
        }
        //compartment floor
        translate([0,60,0]) rotate([0,90,90])  linear_extrude(55) difference() {
           resize([17,67])circle(d=20);
           resize([16,66])circle(d=20); 
           polygon([[0,0],[10,0],[10,-33.5],[-10,-33.5],[-10,33.5],[0,33.5]]);
        }
        //lower cargo begin
        translate([0,0,0]) rotate([0,90,90])  linear_extrude(1) difference() {
           resize([19,69])circle(d=20);
           polygon([[0,0],[10,0],[10,-35],[-10,-35],[-10,35],[0,35]]);
        }
        
        //PRINT
        translate([0,115,0]) roundground(75,rc=rc[1]/*,size=70*/);
}
module storagebox(type=0,roof=true,open=[0,0,0],special=[0,0,0,0],wall=true,slideout=0) {
    //base
    cube([40,190,1]);
    //roof
    if (roof) {
        if (type == 0) {
                translate([9,44,49]) cube([30,96,1]);
            }
            else
            {
                translate([9,44,49]) cube([30,145,1]);
            }
                
    }
    //mattress
    color([0,0,1]) translate([9,38.6,5]) rotate([0,-90*timeframe(slideout,0.75,1)]) translate([-3,0,0]) cube([3,151,39]);
    
    //sides
    translate([0,0,1]) { 
        //right/left
        if (wall) translate([39,0,49]) rotate([0,-open[0]*110]) translate([0,0,-49]) difference() {
            cube([1,95,49]);
            
            rotate([-45,0]) translate([-0.1,-38.5,0]) cube([40.2,35,80]);
        }
        
        translate([39,60,-3]) cube([1,55,3]);
        //lower back
        translate([9,0,0]) cube([30,1,5]);
        //higher back
        translate([9,0,5]) rotate([-45,0]) cube([30,1,62.3]);
        //front
        if (type == 0) {
            translate([11,189,0]) rotate([0,0,open[3]*90]) translate([-1,0,0]) cube([30,1,49]);
        } else translate([9,189,0]) cube([30,1,49]);
        if ((type == 0) || (type == 1))  {
            translate([9,0,0]) {
                translate([0,6.4,10]) cube([30,88.1,1]);
                translate([0,94.5,0]) cube([30,1,48]);
                translate([0,26,30]) cube([30,68.5,1]);
            }
        }
        //right
        if (type == 0) {
            translate([9,0,0]) {
                //2nd spacer
                translate([0,139,0]) cube([30,1,48]);
            }
            //2nd spacer wall
            if (wall) translate([39,94.5,49]) rotate([0,-open[1]*110]) translate([0,0,-49]) cube([1,45,49]);
            translate([9,96,0]) {
                //cooker
                cooker(special);
                //smoke redirector
                if (roof) translate([0,0,30]) rotate([0,-30,0]) cube([34.5,42.5,1]);
            }
            //door side
             translate ([39,139.5,0])rotate([0,timeframe(open[2],0,0.5)*90,0]) {
                 cube([1,50.5,49]);
                translate([0.5,0,48.5]) rotate([0,timeframe(open[2],0.5,0.9)*180,0]) translate([-30-0.5,0,-0.5]) {
                    cube([30,50.5,1]);
                    rotate([0,90,0]) translate([1,3,-8*timeframe(open[2],0.9,1)+1]) {
                        cylinder(14,1.5,1.5);
                        translate([0,44]) cylinder(14,1.5,1.5);
                    }
                }
                
            }
            translate([9,96,0]);
            
        }
        //left
        if (type == 1) {
            translate([9,0,0]) {
                    //2nd spacer
                    translate([0,94.5+32.5,0]) cube([30,1,48]);
                    //1st layer over sink
                    translate([0,94.5,30]) cube([30,33,1]);
                    
            }
            //2nd spacer wall
            if (wall) translate([39,94.5,49]) rotate([0,-open[1]*110]) translate([0,0,-49]) cube([1,33,49]);
            
            //sink
            translate([9,95,0])
            sinkwall(special[1],31.5);
            translate([9+30,95+32.5,0]) rotate([0,0,180])
            sinkwall(special[0],31.5);
            //3rd spacer wall
            if (wall) translate([39,94.5+33,49]) rotate([0,-open[2]*110]) translate([0,0,-49]) cube([1,62.5,49]);
            
        
        }
        
    }
    
}

module ventilation(){
    translate([20.5,0,0]) rotate([0,-90,0]){
        difference() {
            linear_extrude(20.5) polygon([[0,0],[6,-1],[7,7.1325],[0,0]]);
            translate([0,0,0.25]) linear_extrude(20) polygon([[0.25,0.25],[5.75,-0.75],[6.75,7.1325],[0,0.25]]);
            rotate([-10,-90,180]) translate([10.25,1,3]) resize([19,0,5]) rotate([90,0,0]) cylinder(2,2,2,$fn=20);
        }
    }
    translate([0,-1,6]) rotate([7,0,0]) {
        cube([0.25,8.2,0.5]);
        translate([0.25,0,0]) cube([20,0.25,0.5]);
        translate([20.25,0,0]) cube([0.25,8.2,0.5]);
    }
    color([0.5,0.5,0.5]) rotate([10,0,0]){
        for (x = [0,1,2,3,4,5,6,7,8,9,10,11,12]){
        translate([0,0.1,x/2.5+0.52]) rotate([-40,0,0]) cube([20,0.005,0.4]);
        }
    }
}
module bottleholder(){
     rotate([0,-0,-90]) render() union() difference() {
        union() {
            cube([10,10,15]);
            cylinder(15,10,10);
            translate([0,10,0]) cylinder(15,10,10);
        }
        for (x = [0,1,2])    translate([4.5,-3+x*8.1,1])    cylinder(15,4,4);
        translate([-10,-10,-0.01]) cube([10,40,15.02]);
        translate([8,30,7]) rotate([90,0,0])  cylinder(40,6,6);
    }
}
//Animation
module longjohn() {
{
    drive_phase=timeframe($t,0,0.5);
    //drive_phase=0;
    box_phase=timeframe($t,0.5,1);
    //general
    {
        box_walls=true;
        roof=true;
        kullisse=false;
        stand=0;
        //display_rotation=timeframe($t,0,0.5)*360;
    display_rotation=0;
    }
    //box animation
    {
        //logic
        /* Phases:
         * slide, stand, door, lift, doors
         * 1-2,   2-3,  3-5,   5-7,  7-10
        */
        //main box controler
        openstate=tloop(timeframe(box_phase,0,0.1),timeframe(box_phase,0.7,0.8));
        //controls both exit doors
        exit_state=timeframe(openstate,0.3,0.5);
        //doors
        doorstate=timeframe(openstate,0.7,1);
        ///functions
        //slideout
        slideout=timeframe(openstate,0.1,0.2);
        //low cargo doors
        rc=[0,0,0,0];
        //right cargo doors
        openr=[timeframe(doorstate,0,0.3),timeframe(doorstate,0.3,0.6),timeframe(exit_state,0,0.75),timeframe(exit_state,0.75,1)];
        //left cargo doors       
        openl=[timeframe(doorstate,0,0.333),timeframe(doorstate,0.333,0.666),timeframe(doorstate,0.666,1)];
        //right special properties
        specialr=[0,1,2,3];
        //left special properties
        speciall=[timeframe(doorstate,0.666,1),1];
        //stand
        rs=timeframe(openstate,0.2,0.3);
        //roof lift
        roof_lift=timeframe(openstate,0.5,0.7);    
    }
    //drive animation
    {
               
        //steering_degree=-20;
        steering_sets=[
            //drive 2000 1
            [0,2000*timeframe(drive_phase,0,0.1)],
            //turn right 2
            [-20,90*timeframe(drive_phase,0.1,0.2)],
            //drive 4000 3+4
            [0,2000*timeframe(drive_phase,0.2,0.3)],
            [0,2000*timeframe(drive_phase,0.3,0.4)],
            //turn right 5 
            [-20,90*timeframe(drive_phase,0.4,0.5)],
            //drive 2000 6 
            [0,2000*timeframe(drive_phase,0.5,0.6)],
            //turn right 7 
            [-20,90*timeframe(drive_phase,0.6,0.7)],
            //drive 4000 8 + 9
            [0,2000*timeframe(drive_phase,0.7,0.8)],
            [0,2000*timeframe(drive_phase,0.8,0.9)],
            //turn right 10
            [-20,90*timeframe(drive_phase,0.9,01)],
            //end
            [0,0]
        ];
        steering_sets_count=10;
        //distance=rotation*72.5*3.14159/360;
        steering_override=0;
        distance=0;
        //Math
        {
            
            steering_set=steering_sets[round(drive_phase*steering_sets_count+0.5)-1];
            //steering_set=[steering_set_raw[0],steering_set_raw[1]*timeframe(drive_phase,steering_set_raw[2],steering_set_raw[3])];
             steering_degree=steering_set[0];
                        wheel_rotation=drive_phase*360*6000;
            wheel_steering_degree=steering_override+steering_degree;
        }
    }
    
    
//const
    {
        roof_lift_max=50;
    }
    {
        slideoutdist=slideout*30;
        bikestand=max(rs,stand);
        stand_deg=asin((roof_lift*roof_lift_max)/50);
        
    }
}
translate([0,150,0]) rotate([0,0,display_rotation]) translate([0,-150,0]) 
translate([-5,0,0]) translate([0,-2.5,0]) curves(steering_sets=steering_sets,count_max=steering_sets_count,count=0) rotate([0,-steering_degree*0.2,0]) {
    //frame
    {
        translate([0,5,-5]) union() {
            //0
            translate([0,250.65+15,67])rotate([15,0,0]) cube([10,35,10],false);
            //1
            translate([0,250+15,30]) rotate([80,0,0]) cube([10,45.6,10],false);
            //2
            translate([0,50,30]) rotate([0,0,0]) cube([10,215,10]);
        }
        //front fork
        union() translate([5,290+15,49.5]) rotate([15,0,0]) rotate([0,0,wheel_steering_degree]) {
            
            translate([0,0,11]) cylinder(25,3,3);
            difference() {
            translate([0,0,-10]) resize([15,10,60]) rotate([90,90,0]) torus(20,13);
            translate([-20,-10,-42]) cube([40,20,20]);
            }
            translate([-2.5,0,-21]) {
                wheel(wheel_rotation/60*72.5,60,width=5);
                color([0.1,0.1,0.1]) translate([0,0,-11])resize([6,0,19]) mudguard(40,100);
            }
        }     
    }
    //low storage + bike stand    
    translate([0,70,35]) {
        groundframes(rs=bikestand,rc=[rc[2],rc[3]],slideout=slideoutdist);
        mirror([1,0,0]) translate([-10,0,0]) groundframes(rs=bikestand,rc=[rc[0],rc[1]],slideout=slideoutdist);
        translate([5,190,0]) resize([80,12,2]) linear_extrude(2) difference() {
        circle(40);
        polygon([[40,0],[40,-40],[-40,-40],[-40,0]]);
    }
            //static hull
            {
                //lower front
                translate([5,190,0]) resize([0,12,10.1]) difference() {
                    sphere(40);
                    //PRINT
                    sphere(39);
                    translate([-40,-40,0]) cube([80,80,40]);
                    translate([-40,-40,-40]) cube([80,40,40.1]);
                }
                //head mattress
                color([0,0.25,1]) translate([-35+1,1,3]) cube([78,38.6,3]);
                //ground plate
                difference() {
                    translate([-35,0,0]) cube([80,190,2]);
                    translate([44,60,-0.1]) cube([1.1,55,2.2]);
                    translate([-35.1,60,-0.1]) cube([1.1,55,2.2]);
                    
                }
               
                //front
                translate([5,190,2]) resize([0,12,0]) {
                    difference() {
                        cylinder(50,40,40);
                        cylinder(50,35,35);
                        translate([-40,-40,-0.1]) cube([80,40,52.2]);
                    }
                }
              
                {
                    //lower back
                    translate([-35,-1,2]) cube([80,1,6]);
                    //higher back
                    translate([-35,-1,8])difference() {
                         rotate([-45.5,0,0]) cube([80.2,1,61.5+20]);
                        //vent hole         
                        translate([40,-0.1,0]) rotate([-45.5,0,0]) mirror([1,0,0]) translate([10,0,45]) cube([20,10,9]);
                        //head stop
                        translate([40,40,44]) difference() {
                             translate([-41,0,0]) cube([82,20,20]);
                             resize([0,0,11*2]) rotate([-90,0,0]) cylinder(20,39,39);
                         }
                    }
                    //back side
                     translate([0,-1,8]) rotate([-45.5,0,0]){
                             translate([-25.25,0,44.5]) rotate([45.5,0,0]) ventilation();
                         translate([20,0,32])  bottleholder();
                        
                    }
                }
                //roof
                {
                    if (roof) {
                        //rear
                       translate([5,57,62]) rotate([-90+stand_deg,0,0]) cylinder(50,1,1);
                        translate([0,0,roof_lift*roof_lift_max]) {
                            //roof stands
                            {
                                //front right
                                translate([40,194,0]) cylinder(54,1,1);
                                //front left
                                translate([-30,194,0]) cylinder(54,1,1);
                            }
                            
                            //Upper Front
                    translate([5,190,52]) resize([0,12,11]) difference() {
                        sphere(40);
                        //PRINT
                        sphere(39);
                        translate([-40,-40,-40]) cube([80,80,40]);
                        translate([-40,-40,0]) cube([80,40,40.1]);
                    }
                            difference() {
                                
                                translate([5,42,52]) resize([0,0,11]) rotate([-90,0,0])                         difference() {
                                    cylinder(14.5,40,40);
                                    //PRINT
                                    translate([0,0,-0.1]) cylinder(146.2,39,39);
                                    translate([-40,0,-0.1]) cube([80,40.1,190.2]);
                            }
                            translate([-40+5,28,50]) rotate([-45,0,0]) cube([80,10,30]);
                            }
                            translate([5,42+14.5,52])  color([0.14,0.16,0.588]) resize([0,0,11]) rotate([-90,0,0])                         difference() {
                                    cylinder(148-14.5,40,40);
                                    //PRINT
                                    translate([0,0,-0.1]) cylinder(146.2,39,39);
                                    translate([-40,0,-0.1]) cube([80,40.1,190.2]);
                            }
                        }
                    }
                }
            }
            //cargoboxes (slidemagic)
                translate([5+slideoutdist,0,2]) storagebox(0,roof,open=openr,special=specialr,wall=box_walls,slideout=slideout);
                translate([5-slideoutdist,0,2]) mirror([1,0,0])storagebox(1,roof,open=openl,special=speciall,wall=box_walls); 
            
            
    
    }
  
    //bike
    {
        translate([5,90,0]) bike_rear(true,true,wheel_rotation,[1,1,1]);
        //handlebars
        translate([5,98.5,98]) rotate([20,0,0]) rotate([0,0,wheel_steering_degree])  {
            color([0,0,0]) spacer(10);
            translate([0,14,0]) rotate([-20,0,0]) handlebar(false);
        }
    }
}

//translate([-45-50+25,-36,-1]) cube([150,400,1]);

if (kullisse) {
    //translate([-110-1000,-500,-1]) color([0.142,0.045,0.03]) cube([4900+1000,3000,1]);
    translate([-110-1000-100,-2500,-1]) {
        translate([/*-5*/0300,0,0]) color([0.142,0.045,0.03]) cube([7000+5000,6000+5000,1]);
        rotate([10,0,0]) color([0,0,1]) cube([8000,1,4000]);
        translate([7000,-500,0]) rotate([10,0,90]) color([0,0,1]) cube([8000,1,4000]);
         //translate([-7000,-500,-3000]) rotate([10,0,90]) color([0,0,1]) cube([12000,1,7000]);
        
        
    }

    translate([2000,0,0]) rotate([0,0,90]) color([0.76,0.76,0.76]) resize([0,0,1000],auto=true) translate([400,-500,-948.5 ]) import("../ressources/Kulissen/Untitled/Castle_Full.stl",convexity=50);
}
}

//translate([-1000,0,-1500]) color([0,1,1]) cube([1,10000,10000]);

longjohn();