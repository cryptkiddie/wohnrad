use <../rooftent/rooftent.scad>;
use <../LongJohn/LongJohn.scad>;
use <../ressources/modules.scad>;
module storagelifter(animation=$t){
        
    /*
    Animation goes from 1 to 400
    Phases:
    0-40: Lift
    41-70: Pop out
    71-100:Finalise
    */
    //Animation
    {
        //enable two-way
        //distance=0;
        drive_state=timeframe($t,0,0.5);
        //drive_state=0;
        wheel_angle=drive_state*360*10;
        opening_state=tloop(timeframe($t,0.55,0.75),timeframe($t,0.8,0.9));
        distance=drive_state*200*3*5;
        sled_state=timeframe(opening_state,0.25,0.75);
        //Math
        {
            sled_door_state=1-timeframe(sled_state,0,0.3);
        sled_plate_state=1-timeframe(sled_state,0.4,1);
            p0=timeframe(opening_state,0,0.4)*0.4;
            p1=timeframe(opening_state,0.41,0.7)*0.29;
            p2=timeframe(opening_state,0.71,1);
        }
    }
    ///Variables
    {
        lowerBoxHeight=20;
        innerRoomHeight=10;
        upperPartHeight=60;
        steering_angle=-10;
    }
    //calculations
    {
        //Sum of the two static parts
        lowerPartHeight=lowerBoxHeight+innerRoomHeight;
        //Total hight in closed state
        boxHeight=lowerPartHeight+upperPartHeight;
        //calculate current hight for the sides
        sidesLiftHeight=p0/0.41*lowerPartHeight;
        //calculate current hight for the upper box
        topBoxLiftHeight=p0/0.41*upperPartHeight;
        //calcualte angel for the side/roof
        sideFoldAngel=p1/0.29*90+180;
    }
    curve(steering_angle=-steering_angle,
curve_degree=drive_state*360,wheel_distance=192) /*translate([0,distance,0])*/ {
        ////Box
        {
            //move the box to the correct point and hight
            translate ([-60,0,32]){
                {
                    //Not-opening walls
                    union() {
                        ///lower storage
                        //cube([120,190,lowerBoxHeight], false );        
                        {
                            cube([120,190,2]);
                            
                            //upper
                            translate([2,2,lowerBoxHeight-2]) cube([116,186,2]);
                            //left_back
                            cube([2,138,lowerBoxHeight]);
                            //left_front
                            translate([0,180,0]) cube([2,10,lowerBoxHeight]);
                            //front_left_door
                            translate([2,180,0]) rotate([0,0,90+sled_door_state*90]) cube([2,42,lowerBoxHeight]);
                            //front_plate
                            translate([-88+sled_plate_state*90,138,2]) cube([96,40,2]);
                        }
                        //back side
                        cube([120,2,lowerPartHeight], false );
                        //front side
                        translate ([0,188,0]) cube([120,2,lowerPartHeight], false );
                        //right side
                        translate ([118,0,0]) cube([2,190,lowerPartHeight], false );
                    }
                }
                //first layer of movement
                union() translate([0,0,sidesLiftHeight]){
                    ///Side Plates
                    {
                        //Front
                        translate ([-2,190,0]) cube([124,2,boxHeight], false );
                        //Back
                        translate ([-2,-2,0]) cube([124,2,boxHeight], false );
                        //Right
                        translate ([120,0,0]) cube([2,190,boxHeight], false );
                    }
                    //handlebar
                    color ([0.1,0.1,0.1]) {
                        
                        //left block
                        translate([10,-15,90-10]) cube([5,15,10+10]);
                        //right block
                        translate([105,-15,90-10]) cube([5,15,10+10]);
                        //bar
                        translate ([10,-12,95]) rotate([0,90,0]) cylinder(100,2,2);
                    }
                    //second layer of movement
                    translate ([0,0,topBoxLiftHeight+lowerPartHeight]){
                        //Left Side/Roof
                         /*translate ([0,0,upperPartHeight]) rotate ([0,sideFoldAngel,0]) {
                             cube([2,190,boxHeight], false );
                             }
                             translate ([0,0,upperPartHeight]) rotate ([0,sideFoldAngel,0]) {
                             cube([2,190,boxHeight], false );
                             }*/
                        translate([0,0,upperPartHeight]) rotate([0,sideFoldAngel,0]) {
                            cube([2,190,boxHeight]);
                            //translate([0,0,0]) rotate([0,0,0]) translate([0,0,-boxHeight]) cube([2,190,boxHeight]);
                        }
    
                        
                        
                        //upper cargo box
                        {
                            //bottom
                            cube([120,190,2]);
                            //top
                            color([0.14,0.16,0.588]) translate ([0,0,upperPartHeight]) cube([120,190,2]);
                            //roof stands
                            {
                            //back_left
                            cube([2,2,upperPartHeight]);
                            //front_left
                            translate([0,188,0]) cube([2,2,upperPartHeight]);
                            //back_right
                            translate([118,0,0]) cube([2,2,upperPartHeight]);
                            //front_right
                            translate([118,188,0]) cube([2,2,upperPartHeight]);
                            }
                        }
                        
                    }
                }
            }
        }
        ///Bike parts
        {
            
            //wheels
            {
                //front wheels
                //right
                translate ([65,100,36]) {
                    wheel(wheel_angle);
                    color([0.18,0.18,0.18]) mudguard();
                }
                //left
                translate ([-75,100,36]) {
                    wheel(wheel_angle);
                    color([0.18,0.18,0.18]) mudguard();
                }           
            }
            //steering_angle=0;
           translate([0,30,0]) rotate([0,0,steering_angle]) translate([0,-30,0]) bike_rear(true,true,wheel_angle);
             
        }
    }
}
module kullisse() {
    //translate([-110-1000,-500,-1]) color([0.142,0.045,0.03]) cube([4900+1000,3000,1]);
    translate([-110-1000-100,-2500,-1]) {
        translate([/*-5*/0300-1000,0,0]) color([0.142,0.045,0.03]) cube([7000+5000,6000+5000,1]);
        rotate([10,0,0]) color([0,0,1]) cube([8000,1,4000]);
        translate([7000,-500,0]) rotate([10,0,90]) color([0,0,1]) cube([8000,1,4000]);
         //translate([-7000,-500,-3000]) rotate([10,0,90]) color([0,0,1]) cube([12000,1,7000]);
        
        
    }

    //translate([2000,0,0]) rotate([0,0,90]) color([0.76,0.76,0.76]) resize([0,0,1000],auto=true) translate([400,-500,-948.5 ]) import("../ressources/Kulissen/Untitled/Castle_Full.stl",convexity=50);
}

kullisse();

storagelifter();

translate([300,175,0]) longjohn();

translate([600,600,0])  curve(steering_angle=-10,
curve_degree=timeframe($t,0,0.4)*360,wheel_distance=192)  rooftent();

