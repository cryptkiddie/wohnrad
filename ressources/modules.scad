module torus(outerRadius, innerRadius) {
  r=(outerRadius-innerRadius)/2;
  rotate_extrude() translate([innerRadius+r,0,0]) circle(r);
}
module activa(all) {
    rotate([90,0,90]) if (all) {
        import("../ressources/activa.svg");
    } else {
        import("../ressources/activa_frame.svg");
    }
}
module holders(s,a) {
    for (ad=a) {
        translate([0,0,10]) rotate([0,-180+ad+s*(180-ad),0]) difference() {
            cube([100,100,1]);
            translate([0,5,-1]) cube([95,90,3]);
     }
    }
}
module spokes(d,length=60,offset=5) {
    
    color([0.7,0.6,0.6]) for (de=d)     rotate([de,90,0]) translate([-offset,0,-(length/2)])cylinder(length,0.3,0.3);
}
module wheel(r=0,size=72.5,width=10) {
    /*resize([10,72.5,72.5])*/ rotate([-r,0,0]) union() rotate([0,90,0]){
            //tire
            difference() {
                union() {
                    color([0.7,0.6,0.6]) cylinder(width,size/72.5*33,size/72.5*33);
                    color([0,0,0]) translate([0,0,width*0.02]) resize([size,size,width*0.99])   translate([0,0,4.5]) torus(36.25,27);
                }
                translate([0,0,-0.01]) cylinder(width+0.02,size/72.5*29,size/72.5*29);
            }
            
            color([0,0,0]) cylinder(width,size/72.5*5,,size/72.5*5);
            spokes([0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,,150,160,170],size/72.5*60,width/2);
            
            
        }
}
module saddle() {
    difference() {
        union(){
            resize([20,25,11]) sphere(10);
            difference(){
                resize([12,50,10]) sphere(5);
                translate([-10,0,-5.5]) cube([20,25,11]);
            }
        }
        translate([-10,-25,-6.3]) cube([20,40,6]);
    }
}
module bike_rear(insert_wheel=false,insert_mudguard=false,degree=0,framecolor=[0.775,0.299,0.071]) {
    translate([0,0,30]) union() {
            color(framecolor) {
            //Upper tube
            translate([-5,-50,20])rotate([-30,0,0]) cube([10,40,2]);
            //Downtube
            translate([-5,-45,0]) cube([10,100,2]);
            //Seat Post
            translate([6,-59.5,60]) rotate([74,180,0]) cube([12,61,2]);
                }
            
            //Seat
            translate([0,-60,60]) {
                //Post
                color([0.642,0.595,0.595]) rotate([16,0,0]) cylinder(17,1.5,1.5,false);
                //Saddle
                translate([0,-7,12]) rotate([0,0,180]) color([0.1,0.1,0.1]) saddle();
                }
            
            //back wheel mount
            color(framecolor) {
                //upper
                {
                    //right
                    translate([7,-89.3,7.1]) rotate([-60,180,0]) translate([0,0,0]) rear_tube(34.7);
                    //left
                    translate([-7,-88,5.1]) rotate([59.4,0,0]) rear_tube(34.7);
                }
                //lower
                {
                    //right
                    translate([7,-88,7.1]) rotate([7,180,0]) translate([0,0,0]) rear_tube(21);
                    //left
                    translate([-7,-88,5.1]) rotate([-6,0,0]) rear_tube(20);
                }
            }
            //pedals and mechanics
            {
            //axis
            translate ([0,-88,6.25])rotate ([0,90,0]) cylinder(15,2,2,true);
            //axis pedal
            color(framecolor) translate ([0,-40,5])rotate ([0,90,0]) cylinder(20,2,2,true);
            }
        }
        translate([-5,-88,36.25]) {
            if (insert_wheel) wheel(degree);
            if (insert_mudguard) color([0.18,0.18,0.18]) mudguard(-10,170);
    }
}
module rear_tube(length,length2=15){
                            rotate([0,0,20]) cube([2,10,2]);
                            translate([-3.42,9.4,0]) cube([2,length,2]);
                            translate([-3.42,length+9.4,0]) rotate([0,0,-20]) cube([2,length2,2]);
}
module mudguard(r1=0,r2=180,size=72.5,width=10){
    translate([-1,0,0]) rotate([90,-r1,90]) rotate_extrude(angle = r2, convexity = 2) translate([35,0]) polygon(points=[[0,0],[4,4],[4,8],[0,12],[0,11],[3,8],[3,4],[0,1]]);
}


module spacer(length=10) {
     {
        translate([0,1.5,0]) rotate([-90,0,0]) cylinder(length+4,3/2,3/2);
        translate([0,0,-2]) rotate([0,0,0]) cylinder(4,2,2);
        translate([2,length+4,0]) rotate([0,-90,0]) cylinder(4,2,2);
    }
}
module handlebar(racing=false){
    if (racing) {
    } else {
        //main tube
        color([0.2,0.2,0.2]) translate([-31.5,0,0]) rotate([0,90,0]) cylinder(63,1.5,1.5);
        //levers
        //right
        translate([8,1,-0.75]) levers();
        mirror([1,0,0]) translate([8,1,-0.75]) levers();
         //bar ends
        color([0.2,0.2,0.2])  rotate([60,0,0]) difference() {
                translate([0,0,0])  torus(32,28.5);
                translate([-32,-32,-2]) cube([64,32,4]);
                translate([-32,12,-2]) cube([64,32,4]);
        }
    }
}
module cooker(btn_pos=[0,0,0,0]){
    color([0.1,0.1,0.1]) cube([30,42.5,2.45]);
    for (x = [0,1,2,3]) translate([7*x-2.5+7,39,2.5]) {
        rotate([0,0,round(btn_pos[x])*27-90]) color([0.53,0.565,0.567]) difference() {
        cylinder(3,2.5,2.5);
        translate([0.5,-2.5,1.5]) cube([2,5,1.6]);
        translate([-2.5,-2.5,1.5]) cube([2,5,1.6]);
        translate([-0.55,-2.5,1.5]) rotate([40,0,0]) cube([1.1,5,1.6]);
        }
    }
    for (x = [0,1]) for (y = [0,1]) translate([7.5+x*15,10+y*15,2.5]) { 
        color([0.2,0.2,0.2]) cylinder(1,5.5,5.5);
        //burner
        color([0.363,0.363,0.363]) translate([0,0,1]) cylinder(1.5,2,2);
        color([0.1,0.1,0.1]) {
            translate([0,0,4]) difference() {
                cylinder(0.1,4,4);
                translate([0,0,-0.1]) cylinder(1.2,3.5,3.5);
            }
            translate([0,0,4]) difference() {
                cylinder(0.1,,2);
                translate([0,0,-0.1]) cylinder(1.2,1.5,1.5);
            }
            
            for (d = [0,20,40,60,80,100,120,140,160]*2) translate([0,0,2.65]) rotate([20,180,d]) translate([0,-4,0])      cylinder(3.3,0.2,0.2);
            for (d=[0,0.25,0.5,0.75]) {
                translate([0,0,4]) rotate([0,0,d*4*90]) translate([1.8,-0.25,0]) cube([2,0.5,0.1]);
            }
        }
            
    }
}
module roundground(length=10,size=1,rs=0,rc=0) {
    translate([-35,0,0]) rotate([0,rs*90,0]) translate([35,0,0])
    translate([0,0,-10]) rotate([0,rc*-30,0]) translate([0,0,10])
    rotate([-90,0,0]) linear_extrude(length) difference() {
            resize([70,20])circle(d=20);
            resize([70-size,20-size])circle(d=20);
            polygon([[0,0],[0,10.1],[35,10.1],[35,-10.1],[-35,-10.1],[-35,0]]);
    }
    
}
module sinkwall(openstate=0,length=32,height=10) {
    rotate([0,(1-openstate)*45,0]) translate([0,0.5,0]) rotate([90,0,-180]) {
        linear_extrude(0.5) polygon([[0,0],[0,10],[10,10]]);
        translate([0,0,length-0.5]) linear_extrude(0.5) polygon([[0,0],[0,10],[10,10]]);
        translate([0,0,0.5]) linear_extrude(length-0.5) polygon([[0,0],[10,10],[9.5,10],[0,0.5]]);
    }
}
module levers(braking=0,racing=false) {
    if (racing) {
        } else {
            linear_extrude(2) polygon([[0,0],[8,0],[8,4]]);
translate([4.9+3,1.4+1.25+1,0.5]) rotate([0,0,-12*braking]) translate([0,-1.25,0]) linear_extrude(0.5) polygon([[0,1],[1,1],[2,0],[5+2,0],[5.5+2,0.7],[5.3+2,0.7],[5+2,0.5],[2.3,0.5],[1.3,1.5],[0,1.5]]);
        }
    
}
module curve(steering_angle=10,
curve_degree=0,wheel_distance=120){
    if (steering_angle>0) {
        
        //Math
        {
        curve_center=tan(90-steering_angle)*wheel_distance;
        curve_distance=curve_center*2*PI*curve_degree;
        curve_wheel_rotation=curve_distance/(PI*2*36.5);
        }
        translate([-curve_center,0,0])  rotate([0,0,curve_degree]) translate([curve_center,0,0])  children();
    } else if(steering_angle<0) {
        //Math
        {
        curve_center=tan(90-steering_angle)*wheel_distance;
        curve_distance=curve_center*2*PI*curve_degree*(-1);
        curve_wheel_rotation=curve_distance/(PI*2*36.5);
        }
        translate([-curve_center,0,0])  rotate([0,0,-curve_degree]) translate([curve_center,0,0])   children();
    } else  translate([0,curve_degree,0]) children();
}
/*module curves(steering_sets,count_max,count,time=0){
    echo(count);
    if (count > count_max) {
        children();
    } else {
        
        //curve(steering_sets[count][0],steering_sets[count][1]) curves(steering_sets,count=count+1,count_max=count_max,time) children();
        newcount = count +1;
        curves(steering_sets,count= newcount,count_max=count_max,time) children();
       
        
    }
}*/
module curves(count=0,count_max=10,steering_sets=[]){
    if (count == count_max) {
        children();
    } else {
        curve(steering_sets[count][0],steering_sets[count][1]) curves(count+1,count_max,steering_sets) children();
    }
}
{
   
    function timeframe(time,start,end) =(max(start,min(end,time))-start)/(end-start);
    
    function tloop(timeframe1,timeframe2) =min(timeframe1,1-timeframe2);
}