include <../ressources/modules.scad>;
module foldingbed(s,o){
    rotate([0,0,90]) translate([0,-100,0]) {
        translate([0,0,10]) rotate([0,-180+s*180,0])
        difference() {
            cube([100,100,10]);
            translate([1,5,-1]) cube([94,90,9]);
        }
        holders(1-(1-s)*(1-o),[40,80,120,160,180]);
        difference() {
            cube([100,100,10]);
            translate([1,5,1]) cube([94,90,10]);
        }
        translate([0,97.5,5]) rotate([90,0,0]) rotate_extrude(angle = max(0,(180-s*180)*(1-o)), convexity = 4) polygon(points=[[0,0],[100,0],[100,95],[0,95],[0,94],[99,94],[99,1],[0,1]]);
       
    }
}
//variables
module rooftent(){
{
    box_height=80;
}
//translate([0,-110,20]) activa();
//cube([10,10,110]);
//animation
{
    camp_phase=timeframe($t,0.55,1);
    camp_open_phase=timeframe(camp_phase,0.2,0.5);
    camp_close_phase=timeframe(camp_phase,0.7,1);
    fold_angle=tloop(camp_open_phase,camp_close_phase);
    //fold_angle=0;
    opening_angle=0;
    steering_phase=$t;
    steering_angle=10;
    wheel_rotation=timeframe($t,0,0.4);
    //[left,right]
    braking=[1,0];
    front_opening=tloop(camp_open_phase,camp_close_phase);
    //math
    {
        front_opening_t=1-front_opening;
        fold_angle_t=1-fold_angle;
        wheel_angle=wheel_rotation*360;
        //steering_angle=steering_phase*80-40;
    }
}
 {
    //rear
    translate([0,30,0]) rotate([0,0,steering_angle]) translate([0,-50,0]) {
        bike_rear(true,true,wheel_angle);
    }
    //box
    translate([-52,0,32]) {
        //ground
        cube([104,150,2]);
        //front wheels
        {
            translate([106,80,4]) {
               wheel(wheel_angle);
               color([0.18,0.18,0.18]) mudguard();
            }
            translate([-12,80,4]) {
               wheel(wheel_angle);
               color([0.18,0.18,0.18]) mudguard(); 
            }
        }
        //sides
        {
            //left
            translate([0,2,2]) cube([2,146,box_height]);
            //right
            translate([102,2,2]) cube([2,146,box_height]);
            //front
            {
                //static
                translate([0,148,box_height-18]) cube([104,2,20]);
                //opening
                translate([0,150,box_height-18]) rotate([front_opening_t*-110-70,0,0]) translate([0,0,-2]) cube([104,2,box_height-18]);
            }
            //back
            translate([0,0,2]) cube([104,2,box_height-20]);
        }
        //Bett
        translate([2,50,box_height-18]) foldingbed(fold_angle_t,opening_angle);
        //Bar
        {
            translate([-1.5,5,75]) rotate([220+((1-fold_angle_t)*90),0]) {
                translate([0,-2.50,-25]) cube([1,5,30]);
                translate([-1.5,0,0]) rotate([0,90,0]) cylinder(5,1,1);
                translate([106 ,-2.50,-25]) cube([1,5,30]);
                translate([103.5,0,0]) rotate([0,90,0]) cylinder(5,1,1);
                rotate([0,90,0]) translate([22,0,-2]) cylinder(110,1,1);
                
                //levers
                {
                    //right
                    rotate([-220,0,0]) translate([80,-14-0.25,16.5]) rotate([$t*360,0,0]) translate([0,1,-0.25]) levers(braking[1]);
                    //left
                    rotate([-220,0,0]) mirror([1,0,0]) translate([-30,-14,16]) levers(braking[1]);
                }

                
            }
        }
    }
}
}

rooftent();

#translate([-75,-150,-1]) cube([150,350,1]);